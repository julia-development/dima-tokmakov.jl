include("./clear_data.jl")
using HTTP
using Nettle
using Base64
using Dates
using JSON
using DataFrames
using DataFramesMeta


function okx_autarization(path_url)

        timestamp = Dates.format(Dates.now(UTC), "yyyy-mm-ddTHH:MM:SS.sss\\Z")
        message = timestamp*"GET"*path_url
        sign = digest("sha256", ENV["API_SECRET_OKX"], message) |> base64encode

        headers = Dict(
            "OK-ACCESS-KEY" => ENV["API_KEY_OKX"],
            "OK-ACCESS-SIGN" => sign,
            "OK-ACCESS-TIMESTAMP" => timestamp,
            "OK-ACCESS-PASSPHRASE" => ENV["PASS_PHRASE_OKX"])
end

function okx_df()

    base = "https://www.okx.com"
    path_url1 = "/api/v5/asset/currencies"
    path_url2 = "/api/v5/market/tickers?instType=SPOT"

    json_coins = [f for f in get_json(base*path_url1, okx_autarization(path_url1))["data"] if f["mainNet"] == true]
    json_pairs = get_json(base*path_url2)["data"]

    data_coins = DataFrame(map(x -> (symbol = x["ccy"],
                                     coin = x["name"],
                                     chain = x["chain"]), json_coins))
    coins = @rtransform!(data_coins, $[:tresh, :chain]=split(:chain, '-', limit = 2))

    data_pairs = DataFrame(map(x -> (pair = x["instId"],
                                     price = x["last"]), json_pairs))
    pairs = @rtransform!(data_pairs, $[:symbol, :tresh]=split(:pair, '-'))

    df = Dict("coins" => coins[!, Not(r"tresh")],
              "pairs" => pairs[!, Not(r"tresh")],
              "market" => "OKX")
    clear_data(df)
end
