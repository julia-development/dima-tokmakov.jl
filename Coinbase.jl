include("./clear_data.jl")
using HTTP
using JSON
using DataFrames
using DataFramesMeta


function add_prises(pairs)

    url3 = "https://api.exchange.coinbase.com/products/"
    buffer = DataFrame(price = String[])

    for i in 1:nrow(pairs)
        data = get_json(url3*pairs.pair[i]*"/stats")["last"]
        push!(buffer, Dict(:price => data))
    end

    pairs[!, "price"] = buffer[!,"price"]
    return(pairs)
end

function coinbase_df()

    url1 = "https://api.pro.coinbase.com/currencies"
    url2 = "https://api.pro.coinbase.com/products"

    json_coins = get_json(url1)
    json_pairs = [f for f in get_json(url2) if f["status"] == "online"]

    coins = DataFrame(map(x -> (symbol = x["id"],
                                coin = x["name"],
                                chain = x["default_network"]), json_coins))
    pairs = add_prises(DataFrame(map(x -> (pair = x["id"],
                                           symbol = x["base_currency"],
                                           tresh = x["quote_currency"]), json_pairs)))

    df = Dict("coins" => coins,
            "pairs" => pairs[!, Not(r"tresh")],
            "market" => "Coinbase")
    clear_data(df)
end
