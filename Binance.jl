include("./clear_data.jl")
using HTTP
using Dates
using Nettle
using JSON
using DataFrames
using DataFramesMeta


function binance_autarization()

        timestamp = string(1000*round(Int, datetime2unix(Dates.now(UTC))))
        sign = hexdigest("sha256", ENV["API_SECRET_BINANCE"], "timestamp="*timestamp)
end

function binance_df()

        base = "https://api.binance.com"
        path1 = "/sapi/v1/capital/config/getall" #о монетах с именем
        path2 =  "/api/v3/ticker/price" #цена монеты
        path3 = "/api/v3/exchangeInfo"
        headers = Dict(
                "X-MBX-APIKEY" => ENV["API_KEY_BINANCE"],
                "Content-type" => "application/json")
        timestamp = string(1000*round(Int,datetime2unix(Dates.now(UTC))))

        json_coins = get_json(base*path1*"?"*"timestamp="*timestamp*"&signature="*binance_autarization(), headers)
        json_pairs_price = get_json(base*path2)
        json_pairs_tickers = [f for f in get_json(base*path3)["symbols"] if f["status"] == "TRADING"]

        json_coins_with_net = [[f, f["networkList"][i]] for f in json_coins for i in 1:length(f["networkList"])
                if f["networkList"][i]["isDefault"] == true]

        coins = DataFrame(map(x -> (symbol = x[1]["coin"],
                                        coin = x[1]["name"],
                                        chain = x[2]["name"]), json_coins_with_net))
        coins = DataFrame(map(x -> (symbol = x["coin"],
                                            coin = x["name"],
                                            chain = x["networkList"][1]["name"]), json_coins))
        pairs_price = DataFrame(map(x -> (tresh_pair = x["symbol"],
                                            price = x["price"]), json_pairs_price))
        pairs_tickers = DataFrame(map(x -> (tresh_pair = x["symbol"],
                                            pair = x["baseAsset"]*"/"*x["quoteAsset"],
                                            symbol = x["baseAsset"]), json_pairs_tickers))
        pairs = leftjoin(pairs_tickers, pairs_price, on = :tresh_pair; makeunique= true)

        df = Dict("coins" => coins,
                "pairs" => pairs[!, Not(r"tresh_pair")],
                "market" => "Binance")
        clear_data(df)
end
