include("./clear_data.jl")
using HTTP
using JSON
using DataFrames
using DataFramesMeta
using Gumbo
using AbstractTrees


function get_coin_from_html()
   url = "https://support.kraken.com/hc/en-us/articles/360000678446-Cryptocurrencies-available-on-Kraken"

   r = HTTP.request("GET", url)
   body = String(r.body)
   r_parsed = parsehtml(body)

   root = r_parsed.root

   html_tabel = []
   for elem in PreOrderDFS(root)
      try
         if getattr(elem, "class") == "pane-vertical-scroll"
            html_tabel = elem
         end
      catch
         html_tabel
          # Nothing needed here
      end
   end

   coins = DataFrame(coin = String[], symbol = String[], chain = String[])
   buf = Vector{String}(undef, 3)
   i = 1

   for elem in PreOrderDFS(html_tabel)
      try
            if tag(elem) == :td
               if i > 3
                  push!(coins, Dict(:coin => buf[1], :symbol => buf[2], :chain => buf[3]))
                  i = 1
               end
               buf[i] = text(elem)
               i = i + 1
            end
      catch
         coins
          # Nothing needed here
      end
   end

   coins
end

function add_prices(coins)
    base = "https://api.kraken.com"
    path_url3 = "/0/public/Assets"
    buf = DataFrame(symbol = String[])

    for i in 1:nrow(coins)
        data = collect(values(get_json(base*path_url3*"?asset="*coins.symbol[i])["result"]))[1]["altname"]
        push!(buf, Dict(:symbol => data))
    end

    coins[!, "symbol"] = buf[!,"symbol"]
    return(coins)
end

function kraken_df()
   base = "https://api.kraken.com"
   path_url1 = "/0/public/AssetPairs"
   path_url2 = "/0/public/Ticker"
   path_url3 = "/0/public/Assets"

   json_pair_information = collect(values(get_json(base*path_url1)["result"]))
   data_pair_information = DataFrame(map(x -> (pair = x["altname"], price = x["wsname"]), json_pair_information))
   pairs = @rtransform!(data_pair_information, $[:symbol, :tresh]=split(:price, '/'))

   pairs_cash = map(x -> x*",", pairs.pair)
   pairs_cash[end] = split(pairs_cash[end], ",")[1]
   pairs_request = reduce(*, pairs_cash)

   json_pairs_price = collect(values(get_json(base*path_url2*"?pair="*pairs_request)["result"]))
   pairs_price = DataFrame(map(x -> (price = x["c"][1], ), json_pairs_price))

   pairs[!,"price"] = pairs_price[!, "price"]

   df = Dict("coins" => add_prices(get_coin_from_html()) ,
              "pairs" => pairs[!, Not(r"tresh")],
              "market" => "Kraken")
    clear_data(df)
end
