using HTTP
using JSON
using DataFrames
using DataFramesMeta


function clear_data(df)

    df["coins"].chain = map(x -> (replace(uppercase(split(x, "(")[1]), " " => "",
                                                      "-" => "",
                                                      "*" => "",
                                                      "NETWORK" => "",
                                                      "CHAIN" => "",
                                                      "PROTOCOL" => "",
                                                      "ERC20" => "ETHEREUM",
                                                      "BNB SMART CHAIN" => "ETHEREUM",
                                                      "BNB BEACON CHAIN" => "ETHEREUM")), df["coins"].chain)
    df["coins"].coin = map(x -> (replace(uppercase(split(x, "(")[1]), " " => "",
                                                     "-" => "",
                                                     "*" => "",
                                                     "TOKEN" => "",
                                                     "CHAIN" => "",
                                                     "NETWORK" => "",
                                                     "COIN" => "",
                                                     "PROTOCOL" => "")), df["coins"].coin)
    df["pairs"].pair = map(x -> replace(x, "-" => "/"), df["pairs"].pair)
    return(df)
end

function get_json(url, headers = "")

    r = HTTP.request("GET", url, headers = headers)
    perem = r.body |> String
    b = JSON.parse(perem)
end
