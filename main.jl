include("./Binance.jl")
include("./OKX.jl")
include("./Coinbase.jl")
include("./Kraren.jl")
using HTTP
using JSON
using DataFrames
using DataFramesMeta
using YAML


okx_data = okx_df()
coinbase_data = coinbase_df()
kraken_data = kraken_df()
binance_data = binance_df()

arr = [binance_data,okx_data,coinbase_data, kraken_data]



function get_croos_symbols(arr)

    for i in 1:length(arr)
        arr[i]["coins"]  = unique(innerjoin(arr[i]["pairs"], arr[i]["coins"],
            on = :symbol; makeunique= true)[:, ["symbol", "coin", "chain"]])
    end

    coin_join = chain_join = arr[1]["coins"]

    for i in 1:(length(arr)-1)
        coin_join  = outerjoin(coin_join, arr[i+1]["coins"], on = :coin; makeunique= true)
        chain_join = outerjoin(chain_join, arr[i+1]["coins"], on = [:symbol, :chain]; makeunique= true)
    end

    chain_join.symbol = convert(Vector{Union{Missing,String}}, chain_join.symbol)

    for i in 1:(length(arr)-1)
        if i == 1
            chain_join[:, "symbol_"*string(i)] = chain_join[!, "symbol"]
        else
            chain_join[:, "symbol_"*string(i)] = chain_join[!, "symbol_"*string(i-1)]
        end
    end

    for i in 1:length(arr)
        if i == 1
            chain_join[map(ismissing, chain_join[!, "coin"]), ["symbol"]] .= missing
        else
            chain_join[map(ismissing, chain_join[!, "coin_"*string(i-1)]), ["symbol_"*string(i-1)]] .= missing
        end
    end

    coin_join_symbol = DataFrame()
    chain_join_symbol = DataFrame()

    for i in 1:length(arr)
        if i == 1
            coin_join_symbol[!, "symbol"] = coin_join[!, "symbol"]
            chain_join_symbol[!, "symbol"] = chain_join[!, "symbol"]
        else
            coin_join_symbol[!, "symbol_"*string(i-1)] = coin_join[!, "symbol_"*string(i-1)]
            chain_join_symbol[!, "symbol_"*string(i-1)] = chain_join[!, "symbol_"*string(i-1)]
        end
    end

    join_symbol = vcat(coin_join_symbol, chain_join_symbol)

    join_symbol[!, "dmissing_count"] = collect(count(ismissing, c) for c in eachrow(join_symbol))
    join_symbol = filter((:dmissing_count => x -> x < length(arr)-1), join_symbol)

    symbols = Vector{Dict}(undef, length(arr))

    for i in 1:length(arr)
        if i == 1
            symbols[i] = Dict("symbols" => filter(:symbol => !ismissing,
                                           unique(join_symbol[!,[i]])),
                              "pairs" => arr[i]["pairs"],
                              "market" => arr[i]["market"])
        else
            symbols[i] = Dict("symbols" => filter(:symbol => !ismissing,
                                           rename(unique(join_symbol[!,[i]]),
                                           ("symbol_"*string(i-1) => "symbol"))),
                              "pairs" => arr[i]["pairs"],
                              "market" => arr[i]["market"])
        end
    end

    return(symbols)
end

function df_to_yaml(arr)
    symbol_data = innerjoin(arr["symbols"], arr["pairs"], on = :symbol; makeunique= true)

    part_yaml = Dict()
    for i in 1:nrow(symbol_data)
        part_yaml[symbol_data.symbol[i]] = []
    end

    for i in 1:nrow(symbol_data)
        part_yaml[symbol_data.symbol[i]] = vcat(part_yaml[symbol_data.symbol[i]],
                                                Dict(symbol_data.pair[i] => symbol_data.price[i]))
    end
    finish_yaml = Dict(arr["market"] => part_yaml)
end

function to_yaml(arr)
    yaml_f = Vector{Any}(undef, length(arr))
    for i in 1:length(arr)
        yaml_f[i] = df_to_yaml(arr[i])
    end
    YAML.write_file("./data.yaml", yaml_f)
end

to_yaml(get_croos_symbols(arr))
